//
//  TwitterTimeLineViewController.m
//  APMNorthEast
//
//  Created by Tarun Sharma on 16/08/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#import "TwitterTimeLineViewController.h"
#import "UIColor+Hexadecimal.h"
#import "Reachability.h"

@interface TwitterTimeLineViewController ()

@end

@implementation TwitterTimeLineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(triggerAction:) name:kReachabilityChangedNotification object:nil];
    //[self setTwoLineTitle:@"Tweets by FoundationForJobs" color:[UIColor colorWithHex:@"#f4a300"] font:[UIFont boldSystemFontOfSize: 17.0f]];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -80) forBarMetrics:UIBarMetricsDefault];
    [self loadingTweets];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];

    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    leftButton.frame = CGRectMake(0, 0, 10, 20);
    [leftButton addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];

}

-(void) goBack:(UIButton *) sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark LoadingView
-(void)loadingTweets{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        TWTRAPIClient *APIClient = [[TWTRAPIClient alloc] init];
        self.dataSource = [[TWTRUserTimelineDataSource alloc] initWithScreenName:@"Foundation4jobs" APIClient:APIClient];
    }
    else{
        [self alertWithTitle:kAppNameAlert message:kInternetOff actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
    
}

-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    
    
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    [alertCont addAction:okAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
    
}
- (void)dealloc{
    //[super dealloc];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Notification
-(void) triggerAction:(NSNotification *) notification
{
    NSLog (@"Notification Data %@",notification.userInfo);
    if ([[notification name] isEqualToString:@"kNetworkReachabilityChangedNotification"])
    {
        NSLog (@"Successfully received the kNetworkReachabilityChangedNotification!");
        
        [self loadingTweets];
        
        
        
        
    }
    
}
#pragma mark  NavigationTitle

- (void)setTwoLineTitle:(NSString *)titleText color:(UIColor *)color font:(UIFont *)font {
    CGFloat titleLabelWidth = [UIScreen mainScreen].bounds.size.width/2;
    
    UIView *wrapperView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, titleLabelWidth, 44)];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleLabelWidth, 44)];
    label.backgroundColor = [UIColor clearColor];
    label.numberOfLines = 2;
    label.font = font;
    label.adjustsFontSizeToFitWidth = YES;
    label.textAlignment = UIBaselineAdjustmentAlignCenters;
    label.textColor = color;
    label.text = titleText;
    
    [wrapperView addSubview:label];
    
    
    self.navigationItem.titleView = wrapperView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
