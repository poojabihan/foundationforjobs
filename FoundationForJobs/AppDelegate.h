//
//  AppDelegate.h
//  FoundationForJobs
//
//  Created by Alok Mishra on 23/10/17.
//  Copyright © 2017 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <TwitterKit/TwitterKit.h>
#import "Reachability.h"
#import <UserNotifications/UserNotifications.h>
@interface AppDelegate : UIResponder <UIApplicationDelegate,UINavigationControllerDelegate,UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;
@property(nonatomic) BOOL  _isCurrentpageChat;
@property(nonatomic) BOOL  _isQuesFromNotify;
- (void)saveContext;
@property NSString * urlString;
@property (retain, nonatomic)  Reachability* reach;

@end

