//
//  DetailViewController.m
//  TellTheStationMaster
//
//  Created by Tarun Sharma on 09/03/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#import "DetailViewController.h"
#import "Reachability.h"
#import "MBProgressHUD.h"
#import "ImageViewController.h"
#import "UIImageView+WebCache.h"



@interface DetailViewController ()
{
    MBProgressHUD * hud;
    
}
@end

@implementation DetailViewController

#pragma mark ViewLifeCycle Delegates

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationItem setTitle:@"Details"];
    self.imageVw.clipsToBounds = YES;
    
    self.imageVw.contentMode = UIViewContentModeScaleToFill;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (![_imageName isEqualToString:@"noImage"])
        {
            //imageVw.image=[UIImage imageWithData:_imageData];
            // NSString * imageString=[[NSString stringWithFormat:@"%@%@",kImageURL, _imageName]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            
            NSString * imageString = [[NSString stringWithFormat:@"%@%@",kImageURL, _imageName]stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            
            CAShapeLayer * maskLayer = [CAShapeLayer layer];
            maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: self.imageVw
                              .bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomRight | UIRectCornerBottomLeft cornerRadii: (CGSize){8.0, 8.}].CGPath;
            
            self.imageVw.layer.mask = maskLayer;

            
            UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:[NSURL URLWithString:imageString].absoluteString];
            if(image == nil)
            {
                [self.imageVw sd_setImageWithURL:[NSURL URLWithString:imageString] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    if (error == nil) {
                        [self.imageVw setImage:image];
                        //[activityIndicator removeFromSuperview];
                    } else {
                        NSLog(@"Image downloading error: %@", [error localizedDescription]);
                        [self.imageVw setImage:[UIImage imageNamed:@"ImgNotFound.png"]];
                        
                    }
                }];
            } else {
                [self.imageVw setImage:image];
                //[activityIndicator removeFromSuperview];
            }
            
            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                        action:@selector(singleTapGestureCaptured)];
            [self.imageVw addGestureRecognizer:singleTap];
            [self.imageVw setMultipleTouchEnabled:YES];
            [self.imageVw setUserInteractionEnabled:YES];
            
            
        }        else{
            self.imageVw.image=[UIImage imageNamed:@"ImgNotFound.png"];
        }
        
        self.dateAndTime.text = self.date;
        self.detailText.text = self.details;
        self.locationLabel.text=self.locationStr;
        
    });
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    
    
    [self shadowToView:_topView shadow:10.0f];
    [self shadowToView:_bottonView shadow:100.0f];

//    _bottonView.layer.borderColor = UIColor.lightGrayColor.CGColor;
//    _bottonView.layer.borderWidth = 1.0;
    _heightContrsint.constant = 120 + _detailText.frame.size.height;
    
    
    //    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    //    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    //    if(internetStatus != NotReachable)
    //    {
    //    NSURLSession *session=[NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    //
    //    [indicator setTransform:CGAffineTransformMakeScale(1.5f, 1.5f)];
    //    [indicator setColor:[UIColor colorWithRed:255/255.0f green:193/255.0f blue:13/255.0f alpha:1]];
    //
    //    [indicator startAnimating];
    //
    //    [[session dataTaskWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:imageStr]] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
    //      {
    //          [[NSOperationQueue mainQueue] addOperationWithBlock:^{
    //
    //              imageVw.image  = [UIImage imageWithData:data];
    //              [indicator stopAnimating];
    //
    //          }];
    //      }] resume];
    //
    //    dateAndTime.text = date;
    //    detailText.text = details;
    //        locationLabel.text=locationStr;
    //    }
    //    else
    //    {
    //        UIAlertView *myAlert = [[UIAlertView alloc]
    //                                initWithTitle:@"Tell Sid"
    //                                message:@"Internet Connection required."
    //                                delegate:self
    //                                cancelButtonTitle:nil
    //                                otherButtonTitles:@"Ok",nil];
    //        [myAlert show];
    //    }
}

-(void)goBack{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void) shadowToView:(UIView *)currentView shadow:(CGFloat)shadow
{
    currentView.layer.masksToBounds = NO;
    currentView.layer.cornerRadius = 10.0;
    
    currentView.layer.shadowRadius  = shadow;
    currentView.layer.shadowColor   = [UIColor grayColor].CGColor;
    currentView.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    currentView.layer.shadowOpacity = 0.3f;
    currentView.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath1      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(currentView.bounds, shadowInsets)];
    
    currentView.layer.shadowPath    = shadowPath1.CGPath;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
    
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    leftButton.frame = CGRectMake(0, 0, 30, 20);
    [leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:leftButton] animated:YES];

    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // Enable iOS 7 back gesture
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}


-(void)singleTapGestureCaptured{
    hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    // hud.contentColor =[UIColor colorWithRed:255/255.0f green:193/255.0f blue:13/255.0f alpha:1];
    // hud.contentColor =khudColour;
    hud.backgroundView.style = MBProgressHUDBackgroundStyleSolidColor;
    //hud.backgroundView.color = [UIColor colorWithWhite:0.f alpha:0.1f];
    
    // Set the label text.
    hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
    
    // hud.dimBackground = YES;
    
    ImageViewController *image = [self.storyboard instantiateViewControllerWithIdentifier:@"image"];
    image.imageVariable=self.imageVw.image;
    [self.navigationController pushViewController:image animated:YES];
    
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
    
}
#pragma mark Gesture Delegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}

@end
