//
//  NewsDetailsViewController.h
//  APMNorthEast
//
//  Created by Tarun Sharma on 18/08/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsDetailsViewController : UIViewController<UIScrollViewDelegate>
{
    IBOutlet UIPageControl *pageControl;
    
    IBOutlet UIScrollView *imagesScrollView;
}
@property (strong, nonatomic) IBOutlet UIScrollView *parentScrollView;
@property (strong, nonatomic) IBOutlet UIView *upperView;
@property (strong, nonatomic) IBOutlet UIView *lowerView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *upperViewHeightConstraint;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *lowerViewHeightConstraint;

- (IBAction)changePage:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *newsTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *subTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *summaryTextLabel;
@property NSString * eventTitleString, *subtitleString,*summaryString,*imageURL;
@property NSMutableArray * imagesArray;
@end
