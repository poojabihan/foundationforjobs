//
//  EventListTableVC.h
//  APMNorthEast
//
//  Created by Tarun Sharma on 18/08/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventListTableVC : UIViewController
@property NSMutableArray * eventBookingArray;
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property NSString * getCurrentEventsURL, *eventMainImageURL;
@end
