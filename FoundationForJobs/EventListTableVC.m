//
//  EventListTableVC.m
//  APMNorthEast
//
//  Created by Tarun Sharma on 18/08/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#import "EventListTableVC.h"
#import "MBProgressHUD.h"
#import "UIColor+Hexadecimal.h"
#import "Reachability.h"
#import "Webservice.h"
#import "EventCell.h"
#import "EventDetailsViewController.h"
#import "UIImageView+WebCache.h"

@interface EventListTableVC ()
{
    MBProgressHUD * hud;
}
@end

@implementation EventListTableVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _tblView.estimatedRowHeight = 145;

    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    leftButton.frame = CGRectMake(0, 0, 30, 20);
    [leftButton addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
   // self.view.backgroundColor = [UIColor colorWithHex:@"f4a300"];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -80) forBarMetrics:UIBarMetricsDefault];
    //[self setTwoLineTitle:@"Event List" color:[UIColor colorWithHex:@"#f4a300"] font:[UIFont boldSystemFontOfSize: 17.0f]];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(triggerAction:) name:kReachabilityChangedNotification object:nil];
    [self loadingElementsInEventList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) goBack:(UIButton *) sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSString *)changeDate:(NSString *)strDate {
    NSString *dateString = strDate;
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [format dateFromString:dateString];
    [format setDateFormat:@"dd,MMM yyyy"];
    NSString* finalDateString = [format stringFromDate:date];
    return finalDateString;
}



#pragma mark LoadingView
-(void)loadingElementsInEventList{
    self.eventBookingArray=[[NSMutableArray alloc]init];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        
        // Do any additional setup after loading the view.
        
        hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        
        // Set the label text.
        hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
        
        self.getCurrentEventsURL=[NSString stringWithFormat:@"%@Eventsapi/events_list/",kEventsBaseURL];
        
        [Webservice requestPostUrl:self.getCurrentEventsURL parameters:nil success:^(NSDictionary *response) {
            NSLog(@"response:%@",response);
            if ([[response objectForKey:@"response"] isEqualToString:@"Succ"]) {
                self.eventBookingArray=[NSMutableArray arrayWithArray:[response objectForKey:@"record"]];
                
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                    
                    [self.tblView reloadData];
                    
                });
                
                
            }
            
            else if (response==NULL || [[[response objectForKey:@"error"]objectForKey:@"error"]isEqualToString:@"Something is problem!."]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self.tblView reloadData];
                    
                    [hud hideAnimated:YES];
                });
                
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                
                
                
                NSLog(@"response is null");
                
                
            }
            else if ([[response objectForKey:@"response"] isEqualToString:@"Fail"] && [[[response objectForKey:@"error"] objectForKey:@"error"] isEqualToString:@"Record not found."]){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.tblView reloadData];
                    
                    [hud hideAnimated:YES];
                });
                
                
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                });
                
                
            }
            
            
        } failure:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self.tblView reloadData];
                
                [hud hideAnimated:YES];
            });
            NSLog(@"Error %@",error);
        }];
        
        
        
        
    }
    else{
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.tblView reloadData];
            
        });
        
        [self alertWithTitle:kAppNameAlert message:kInternetOff actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
}

#pragma mark  Alert Method

-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    
    
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    [alertCont addAction:okAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
    
}

#pragma mark  Reachability Delegates

-(void) triggerAction:(NSNotification *) notification
{
    NSLog (@"Notification Data %@",notification.userInfo);
    if ([[notification name] isEqualToString:@"kNetworkReachabilityChangedNotification"])
    {
        NSLog (@"Successfully received the kNetworkReachabilityChangedNotification!");
        [self loadingElementsInEventList];
        //[self returnDate:self.bookingCalendar.currentPage];
        
        
    }
    
}
- (void)dealloc
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"%s",__FUNCTION__);
}
- (void)setTwoLineTitle:(NSString *)titleText color:(UIColor *)color font:(UIFont *)font {
    CGFloat titleLabelWidth = [UIScreen mainScreen].bounds.size.width/2;
    
    UIView *wrapperView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, titleLabelWidth, 44)];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleLabelWidth, 44)];
    label.backgroundColor = [UIColor clearColor];
    label.numberOfLines = 2;
    label.font = font;
    label.adjustsFontSizeToFitWidth = YES;
    label.textAlignment = UIBaselineAdjustmentAlignCenters;
    label.textColor = color;
    label.text = titleText;
    [wrapperView addSubview:label];
    
    
    self.navigationItem.titleView = wrapperView;
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.eventBookingArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    EventCell * eventCell= [tableView dequeueReusableCellWithIdentifier:@"EventCell" forIndexPath:indexPath];
    
    //eventCell.backgroundColor=[UIColor colorWithHex:@"f4a300"];
    if (eventCell == nil) {
        eventCell = [[EventCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"EventCell"];
    }
    
    
    eventCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    eventCell.eventTitleLabel.text=[[self.eventBookingArray objectAtIndex:indexPath.row]objectForKey:@"title"];
    
    eventCell.dateLabel.text=[self changeDate:[[self.eventBookingArray objectAtIndex:indexPath.row]objectForKey:@"date"]];
    
    
    
    eventCell.locationLabel.text=[[self.eventBookingArray objectAtIndex:indexPath.row]objectForKey:@"venue"];
    
    eventCell.organizerLabel.text=[[self.eventBookingArray objectAtIndex:indexPath.row]objectForKey:@"organiser"];
    
    NSMutableArray * imagesArray = [[NSMutableArray alloc] initWithArray:[[self.eventBookingArray objectAtIndex:indexPath.row]objectForKey:@"imagearray"]];
    
    self.eventMainImageURL= [[NSString stringWithFormat:@"%@%@",kEventImageURL, [imagesArray objectAtIndex:0]]stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:[NSURL URLWithString:self.eventMainImageURL].absoluteString];
    if(image == nil)
    {
        
        [eventCell.imgEvent sd_setImageWithURL:[NSURL URLWithString:self.eventMainImageURL] placeholderImage:[UIImage imageNamed:@"ImgNotFound.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (error == nil) {
                [eventCell.imgEvent setImage:image];
                
            } else {
                NSLog(@"Image downloading error: %@", [error localizedDescription]);
                [eventCell.imgEvent setImage:[UIImage imageNamed:@"ImgNotFound.png"]];
                
            }
        }];
    } else {
        [eventCell.imgEvent setImage:image];
        
    }
    return eventCell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   return UITableViewAutomaticDimension;
}
#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"eventDetailsSegueFromEL"]) {
        NSIndexPath * indexPath = [self.tblView indexPathForSelectedRow];
        
        EventDetailsViewController * eventDetailVC = [segue destinationViewController];
        
        [eventDetailVC setEventTitleString:[[self.eventBookingArray objectAtIndex:indexPath.row]objectForKey:@"title"]];
        [eventDetailVC setSubtitleString:[[self.eventBookingArray objectAtIndex:indexPath.row]objectForKey:@"sub_title"]];
        [eventDetailVC setDateString:[[self.eventBookingArray objectAtIndex:indexPath.row]objectForKey:@"date"]];
        [eventDetailVC setTimeString:[[self.eventBookingArray objectAtIndex:indexPath.row]objectForKey:@"time"]];
        [eventDetailVC setVenueString:[[self.eventBookingArray objectAtIndex:indexPath.row]objectForKey:@"venue"]];
        [eventDetailVC setCpdString:[[self.eventBookingArray objectAtIndex:indexPath.row]objectForKey:@"cpd"]];
        [eventDetailVC setCostString:[[self.eventBookingArray objectAtIndex:indexPath.row]objectForKey:@"cost"]];
        [eventDetailVC setOrganizerString:[[self.eventBookingArray objectAtIndex:indexPath.row]objectForKey:@"organiser"]];
        [eventDetailVC setSummaryString:[[self.eventBookingArray objectAtIndex:indexPath.row]objectForKey:@"summary_text"]];
        [eventDetailVC setImagesArray:[[self.eventBookingArray objectAtIndex:indexPath.row]objectForKey:@"imagearray"]];
        eventDetailVC.BookUrl=[[self.eventBookingArray objectAtIndex:indexPath.row] objectForKey:@"url"];
    }
}

@end
