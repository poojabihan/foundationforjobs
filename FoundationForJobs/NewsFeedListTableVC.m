//
//  NewsFeedListTableVC.m
//  APMNorthEast
//
//  Created by Tarun Sharma on 18/08/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#import "NewsFeedListTableVC.h"
#import "MBProgressHUD.h"
#import "UIColor+Hexadecimal.h"
#import "Reachability.h"
#import "Webservice.h"
#import "NewsFeedCell.h"
#import "UIImageView+WebCache.h"
#import "NewsDetailsViewController.h"
#import "UIColor+Hexadecimal.h"
@interface NewsFeedListTableVC ()
{
    MBProgressHUD * hud;
}
@end

@implementation NewsFeedListTableVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
   // self.view.backgroundColor=[UIColor colorWithHex:@"f4a300"];
    
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    leftButton.frame = CGRectMake(0, 0, 30, 20);
    [leftButton addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];

//    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -80) forBarMetrics:UIBarMetricsDefault];
    
    //[self setTwoLineTitle:@"News" color:[UIColor colorWithHex:@"#f4a300"] font:[UIFont boldSystemFontOfSize: 17.0f]];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(triggerAction:) name:kReachabilityChangedNotification object:nil];
    
    [self loadingElementsInEventList];
    self.tblView.estimatedRowHeight = 120;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) goBack:(UIButton *) sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark LoadingView
-(void)loadingElementsInEventList{
    self.newsListArray=[[NSMutableArray alloc]init];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        
        // Do any additional setup after loading the view.
        
        
        
        hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        
        
        // Set the label text.
        hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
        
        
        self.getCurrentNewsURL=[NSString stringWithFormat:@"%@Eventsapi/news_list",kEventsBaseURL];
        
        [Webservice requestPostUrl:self.getCurrentNewsURL parameters:nil success:^(NSDictionary *response) {
            NSLog(@"response:%@",response);
            if ([[response objectForKey:@"response"] isEqualToString:@"Succ"]) {
                self.newsListArray=[NSMutableArray arrayWithArray:[response objectForKey:@"record"]];
                
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                    
                    [self.tblView reloadData];
                    
                });
                
                
            }
            
            else if (response==NULL || [[[response objectForKey:@"error"]objectForKey:@"error"]isEqualToString:@"Something is problem!."]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self.tblView reloadData];
                    
                    [hud hideAnimated:YES];
                });
                
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
                
                
                
                NSLog(@"response is null");
                
                
            }
            else if ([[response objectForKey:@"response"] isEqualToString:@"Fail"] && [[[response objectForKey:@"error"] objectForKey:@"error"] isEqualToString:@"Record not found."]){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.tblView reloadData];
                    
                    [hud hideAnimated:YES];
                });
                
                
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [hud hideAnimated:YES];
                });
                
                
            }
            
            
        } failure:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self.tblView reloadData];
                
                [hud hideAnimated:YES];
            });
            NSLog(@"Error %@",error);
        }];
        
        
        
        
    }
    else{
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.tblView reloadData];
            
        });
        
        [self alertWithTitle:kAppNameAlert message:kInternetOff actionTitle:@"Dismiss" actionStyle:UIAlertActionStyleCancel];
    }
}

#pragma mark  Alert Method

-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    
    
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    [alertCont addAction:okAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
    
}

#pragma mark  Reachability Delegates

-(void) triggerAction:(NSNotification *) notification
{
    NSLog (@"Notification Data %@",notification.userInfo);
    if ([[notification name] isEqualToString:@"kNetworkReachabilityChangedNotification"])
    {
        NSLog (@"Successfully received the kNetworkReachabilityChangedNotification!");
        [self loadingElementsInEventList];
        //[self returnDate:self.bookingCalendar.currentPage];
        
        
    }
    
}
- (void)dealloc
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"%s",__FUNCTION__);
}
- (void)setTwoLineTitle:(NSString *)titleText color:(UIColor *)color font:(UIFont *)font {
    CGFloat titleLabelWidth = [UIScreen mainScreen].bounds.size.width/2;
    
    UIView *wrapperView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, titleLabelWidth, 44)];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleLabelWidth, 44)];
    label.backgroundColor = [UIColor clearColor];
    label.numberOfLines = 2;
    label.font = font;
    label.adjustsFontSizeToFitWidth = YES;
    label.textAlignment = UIBaselineAdjustmentAlignCenters;
    label.textColor = color;
    label.text = titleText;
    [wrapperView addSubview:label];
    
    
    self.navigationItem.titleView = wrapperView;
}

- (NSString *)changeDate:(NSString *)strDate {
    NSString *dateString = strDate;
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [format dateFromString:dateString];
    [format setDateFormat:@"dd,MMM yyyy"];
    NSString* finalDateString = [format stringFromDate:date];
    return finalDateString;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.newsListArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NewsFeedCell * newsCell= [tableView dequeueReusableCellWithIdentifier:@"NewsFeedCell" forIndexPath:indexPath];
    
     newsCell.selectionStyle = UITableViewCellSelectionStyleNone;
   // newsCell.backgroundColor = [UIColor colorWithHex:@"f4a300"];
    
    if (newsCell == nil) {
        newsCell = [[NewsFeedCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"NewsFeedCell"];
    }
    
    newsCell.newsTitleLabel.text=[[self.newsListArray objectAtIndex:indexPath.row]objectForKey:@"title"];
    
    newsCell.dateLabel.text=[self changeDate:[[self.newsListArray objectAtIndex:indexPath.row]objectForKey:@"date"]];
    
    if ([[[self.newsListArray objectAtIndex:indexPath.row]objectForKey:@"image"] isEqualToString:@""]) {
        
        UIImage *tmpImage = [UIImage imageNamed:@"ImgNotFound.png"];
        newsCell.mainImageView.image = tmpImage;
        
    }
    else{
        newsCell.mainImageView.image=nil;
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [activityIndicator setCenter: newsCell.mainImageView.center];
        [activityIndicator startAnimating];
        activityIndicator.color=[UIColor colorWithCGColor:khudColour.CGColor];
        [newsCell.contentView addSubview:activityIndicator];
        //imageURLHist =[[NSString stringWithFormat:@"%@%@",kImageURL, [dictForTV objectForKey:@"images"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSString *front_image = [NSString stringWithFormat:@"%@",[[[self.newsListArray objectAtIndex:indexPath.row] objectForKey:@"descarray"] objectAtIndex:0]];
        //       NSLog(@"image front page - %@",front_image);
        //
        //
        //        if (front_image==nil) {
        //
        //            [newsCell.mainImageView setImage:[UIImage imageNamed:@"ImgNotFound.png"]];
        //
        //        }else{
        //
        //            [newsCell.mainImageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kEventImageURL,[[[self.newsListArray objectAtIndex:indexPath.row] objectForKey:@"descarray"] objectAtIndex:0]]]];
        //            [activityIndicator stopAnimating];
        //        }
        
        
        self.newsMainImageURL= [[NSString stringWithFormat:@"%@%@",kEventImageURL, [[self.newsListArray objectAtIndex:indexPath.row]objectForKey:@"image"]]stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:[NSURL URLWithString:self.newsMainImageURL].absoluteString];
        
        if(image == nil)
        {
            if (!(front_image==nil)) {
                [newsCell.mainImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kEventImageURL,[[[self.newsListArray objectAtIndex:indexPath.row] objectForKey:@"descarray"] objectAtIndex:0]]]];
                [activityIndicator stopAnimating];
            }
            else{
                [newsCell.mainImageView sd_setImageWithURL:[NSURL URLWithString:self.newsMainImageURL] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    if (error == nil) {
                        [newsCell.mainImageView setImage:image];
                        [activityIndicator stopAnimating];
                        //[activityIndicator removeFromSuperview];
                    } else {
                        NSLog(@"Image downloading error: %@", [error localizedDescription]);
                        [newsCell.mainImageView setImage:[UIImage imageNamed:@"ImgNotFound.png"]];
                        [activityIndicator stopAnimating];
                    }
                }];
            }
        } else {
            [newsCell.mainImageView setImage:image];
            [activityIndicator stopAnimating];
            //[activityIndicator removeFromSuperview];
        }
    }
    
    
    
    
    
    
    
    return newsCell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 145;
}
#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([[segue identifier] isEqualToString:@"newsDetailsSegueFromNL"]) {
        NSIndexPath * indexPath = [self.tblView indexPathForSelectedRow];
        
        NewsDetailsViewController * newsDetailVC = [segue destinationViewController];
        
        [newsDetailVC setEventTitleString:[[self.newsListArray objectAtIndex:indexPath.row]objectForKey:@"title"]];
        [newsDetailVC setSubtitleString:[[self.newsListArray objectAtIndex:indexPath.row]objectForKey:@"date"]];
        [newsDetailVC setSummaryString:[[self.newsListArray objectAtIndex:indexPath.row]objectForKey:@"description"]];
        [newsDetailVC setImagesArray:[[self.newsListArray objectAtIndex:indexPath.row]objectForKey:@"descarray"]];
        
    }
}

@end
