//
//  ViewController.m
//  TellTheStationMaster
//
//  Created by Tarun Sharma on 09/03/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#import "ViewController.h"
#import "HistoryViewController.h"
#import "Reachability.h"
#import "MessagesViewController.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "ChatViewController.h"
#import "SCLAlertView.h"
#import "UIColor+Hexadecimal.h"
@import Firebase;
#define MAX_HEIGHT 100

@interface ViewController ()
{
    NSString * messageInTVStr;
    UIImage *chosenImage;
    UITextView  *commentTxtView1;
    NSString *longitude, *latitude;
    MBProgressHUD *hud, * pleaseWaitHud,*registeringHUD;
    NSString *imgString;
    NSArray * dictArray,* inboxDictArray, * itemChatArray,*resultArray,*questionDataArray;
    NSString * inboxStr;
    SCLAlertView *customAlert;
    UITextView * textViewInAlert;

}
@property (strong, nonatomic) CLLocationManager *locationManager;

@end
NSManagedObjectContext * messageContextTS,* chatContextTS ;

@implementation ViewController

#pragma mark ViewLifecycle Delegates

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"View DID Load Called");
    
    if ([self hasTopNotch]) {
        _logoTopConstraint.constant = 40.0;
    }
    else {
        _logoTopConstraint.constant = 25.0;
    }

   // [self setTwoLineTitle:@"Feedback" color:[UIColor colorWithHex:@"#f4a300"] font:[UIFont boldSystemFontOfSize: 17.0f]];
    
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    leftButton.frame = CGRectMake(0, 0, 30, 20);
    [leftButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:leftButton] animated:YES];

#pragma mark - ragister firebase userdefault test
    
    [[NSUserDefaults standardUserDefaults]setValue:@"yes" forKey:@"registered"];
    
   // self.navigationController.navigationBar.hidden=YES;
    self.navigationController.delegate=self;
    self.messageButton.layer.cornerRadius = 8; // this value vary as per your desire
    self.messageButton.clipsToBounds = YES;
    self.historyButton.layer.cornerRadius = 8; // this value vary as per your desire
    self.historyButton.clipsToBounds = YES;
    
    self.sendButton.layer.cornerRadius = 10; // this value vary as per your desire
    self.sendButton.clipsToBounds = YES;
    self.takeAndSendButtonInstance.layer.cornerRadius = 10; // this value vary as per your desire
    self.takeAndSendButtonInstance.clipsToBounds = YES;



    self.commentTxtView.delegate = self;
    
    [self.commentTxtView.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    self.commentTxtView.layer.cornerRadius = 5.0;
    [self.commentTxtView.layer setBorderWidth:1.0f];
    [self.commentTxtView setKeyboardType:UIKeyboardTypeASCIICapable];

   
    [self shadowToView:_view1];
    [self shadowToView:_view2];
    [self shadowToView:_view3];
    
    [self shadowToButton:_messageButton];
    [self shadowToButton:_historyButton];
    [self shadowToButton:_sendButton];
    
    
    [self.commentTxtView setKeyboardType:UIKeyboardTypeASCIICapable];

    self.rghtButton.tag = 0;
    //AppDelegate *app=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    //historyContextTS=[UIAppDelegate managedObjectContext];

    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    // getting an Email String
    NSString *emailIdString = [standardUserDefaults stringForKey:@"emailId"];
    
    NSString * isRegistered=[standardUserDefaults stringForKey:@"registered"];
    
    NSLog(@"email id in view did load %@ and is registered %@",emailIdString,isRegistered);
    
}

-(void)goBack{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void) shadowToView:(UIView *)currentView
{
    currentView.layer.masksToBounds = NO;
    currentView.layer.cornerRadius = 15.0;
    
    currentView.layer.shadowRadius  = 10.0f;
    currentView.layer.shadowColor   = [UIColor blackColor].CGColor;
    currentView.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    currentView.layer.shadowOpacity = 0.3f;
    currentView.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath1      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(currentView.bounds, shadowInsets)];
    
    currentView.layer.shadowPath    = shadowPath1.CGPath;
    
}

- (void) shadowToButton:(UIButton *)currentButton
{
    currentButton.layer.shadowRadius  = 5.0f;
    currentButton.layer.shadowColor   = [UIColor colorWithRed:207.0f green:3.0f blue:84.0f alpha:.0].CGColor;
    currentButton.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    currentButton.layer.shadowOpacity = 0.6f;
    currentButton.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath1      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(currentButton.bounds, shadowInsets)];
    currentButton.layer.shadowPath    = shadowPath1.CGPath;
    
}

- (void)setTwoLineTitle:(NSString *)titleText color:(UIColor *)color font:(UIFont *)font {
    
    CGFloat titleLabelWidth = [UIScreen mainScreen].bounds.size.width/2;
    
    UIView *wrapperView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, titleLabelWidth, 44)];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleLabelWidth, 44)];
    label.backgroundColor = [UIColor clearColor];
    label.numberOfLines = 2;
    label.font = font;
    label.adjustsFontSizeToFitWidth = YES;
    label.textAlignment = UIBaselineAdjustmentAlignCenters;
    label.textColor = color;
    label.text = titleText;
    [wrapperView addSubview:label];
    
    self.navigationItem.titleView = wrapperView;
    
}

#pragma mark Navigation method

- (id <UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC
{
    NSLog(@"from VC class %@", [fromVC class]);
    if ([fromVC isKindOfClass:[MessagesViewController class]])
    {
        NSLog(@"Returning from Messages popped controller");
        self.stringToCheckVC=@"Messages";
    }
    else if([fromVC isKindOfClass:[HistoryViewController class]]){
        NSLog(@"Returning from History popped controller");
        self.stringToCheckVC=@"History";
    }
    else if ([fromVC isKindOfClass:[ChatViewController class]]){
        NSLog(@"Returning from chatView ");
        self.stringToCheckVC=@"Chat";
        
    }
    return nil;
}

#pragma mark ScrollView Method

-(void)viewDidLayoutSubviews{
    
    [self.scrollViewInVC setContentSize:CGSizeMake(self.scrollViewInVC.frame.size.width, self.sendButton.frame.origin.y + self.sendButton.frame.size.height + 10)];
}


#pragma mark TextView Delegates

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if((self.commentTxtView.text.length == 0) && (commentTxtView1.text.length == 0))
    {
        _textField1.hidden = false;
        
        self.commentTxtView.backgroundColor = [UIColor clearColor];
        commentTxtView1.backgroundColor = [UIColor clearColor];
    }
    else
    {
        _textField1.hidden = true;
        
        self.commentTxtView.backgroundColor = [UIColor whiteColor];
        commentTxtView1.backgroundColor = [UIColor whiteColor];
    }

    
    return YES;
}

-(void)textViewDidChange:(UITextView *)textView
{
    if((self.commentTxtView.text.length == 0) && (commentTxtView1.text.length == 0))
    {
            _textField1.hidden = false;
        
        self.commentTxtView.backgroundColor = [UIColor clearColor];
        commentTxtView1.backgroundColor = [UIColor clearColor];
    }
    else
    {
        _textField1.hidden = true;
        
        self.commentTxtView.backgroundColor = [UIColor whiteColor];
        commentTxtView1.backgroundColor = [UIColor whiteColor];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

#pragma mark TextField Delegate

-(BOOL) textFieldShouldReturn: (UITextField *) textField
{
    
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark Gesture Delegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}

#pragma mark SendingEmail Methods

- (BOOL)validateEmailWithString:(NSString*)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (IBAction)onSendingMail:(id)sender
{
    
    hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    //hud.contentColor =[UIColor colorWithRed:255/255.0f green:193/255.0f blue:13/255.0f alpha:1];
    //hud.contentColor =khudColour;
    hud.backgroundView.style = MBProgressHUDBackgroundStyleSolidColor;
    //hud.backgroundView.color = [UIColor colorWithWhite:0.f alpha:0.1f];
    
    // Set the label text.
    hud.label.text = NSLocalizedString(@"Sending...", @"HUD loading title");
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        
        // getting an Email String
        NSString *emailIdString = [standardUserDefaults stringForKey:@"emailId"];
        if (emailIdString==NULL) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES];
            });
            
            }else{
            
            messageInTVStr = [self.commentTxtView text];
            
            messageInTVStr = [messageInTVStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            [self sendingMail];

        }
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [hud hideAnimated:YES];
        });
        [self errorAlertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"Dismiss"];
        
    }
    
}

-(void)sendingMail
{
   
    
    
    
    if([messageInTVStr length] > 0)
    {
       // [indicator startAnimating];
        
        
        NSString *emailIdString;
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        // saving an NSString
        
        if([self image:self.imageVw.image isEqualTo:[UIImage imageNamed:@"CameraNotAllowed.png"]]==YES){
            imgString=@"";
            
        }
        else{
            NSData *imageData = UIImageJPEGRepresentation(self.imageVw.image, 0.2);
            
            imgString = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
            
        }
        
        // getting an email NSString
        emailIdString = [standardUserDefaults stringForKey:@"emailId"];
       
        NSLog(@"email id %@",emailIdString);
       
        
        //NSString *post = [NSString stringWithFormat:@"msg_detail=%@&email_id_to=%@&location_detail=%@&device_id=%@&email_id=%@&images=%@&latitude=%@&longitude=%@&ssecrete=%@&app_name=%@&fcm_token=%@",messageInTVStr,emailIdString,locationText.text,[[[UIDevice currentDevice] identifierForVendor] UUIDString],@"ganesh@chetaru.com",imgString,latitude,longitude,@"tellsid@1",kAppNameAPI,refreshedToken];
        
        self.refreshedToken=[[FIRInstanceID instanceID]token];
        
        NSLog(@"FCMTokenString %@",self.refreshedToken);
        
        
        NSDictionary *dictParam = @{@"msg_detail":messageInTVStr?: @"",@"email_id_to":emailIdString,@"location_detail":self.locationText.text?: @"" ,@"device_id":[[[UIDevice currentDevice] identifierForVendor] UUIDString],@"images":imgString?: @"",@"latitude":latitude?: @"",@"longitude":longitude?: @"",@"app_name":kAppNameAPI,@"fcm_token":_refreshedToken};
        //@"enquiries@thechangeconsultancy.co"
        
        NSLog(@"Dic %@",_refreshedToken);
        
        self.itemPostURL=[NSString stringWithFormat:@"%@postdetails/",FBaseURL];
   
        [Webservice requestPostUrl:self.itemPostURL parameters:dictParam success:^(NSDictionary *response) {
            NSLog(@"Response : %@",response);
             if ([[response objectForKey:@"response"] isEqualToString:@"Succ"]) {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [hud hideAnimated:YES];
                     
                     
                     [self errorAlertWithTitle:kAppNameAlert message:@"Thank you. You'll receive a confirmation within 24 hours." actionTitle:@"Done"];
                     
                     
                     self.commentTxtView.text = @"";
                     
                     self.commentTxtView.backgroundColor = [UIColor clearColor];
                     _textField1.hidden = false;
                     
                     self.locationText.text = @"";
                     self.imageVw.image = [UIImage imageNamed:@"CameraNotAllowed.png"];
                     [self.rghtButton setBackgroundImage:[UIImage imageNamed:@"feedbackpin.png"] forState:UIControlStateNormal];
                     self.deleteButton.hidden=YES;
                     
                 });
             }
            
           else if (response == NULL) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [hud hideAnimated:YES];
                    });
                    
                    [self errorAlertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss"];
                    
                
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [hud hideAnimated:YES];
                });
                
                [self errorAlertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss"];

            }
        } failure:^(NSError *error) {
            NSLog(@"Error: %@", error);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [hud hideAnimated:YES];
                });
                
                [self errorAlertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss"];
                
            
        }];
        
        

    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [hud hideAnimated:YES];
        });
        
        
        [self errorAlertWithTitle:kAppNameAlert message:@"Please enter text." actionTitle:@"OK"];
    }
     
    
}

- (BOOL)image:(UIImage *)image1 isEqualTo:(UIImage *)image2
{
    NSData *data1 = UIImagePNGRepresentation(image1);
    NSData *data2 = UIImagePNGRepresentation(image2);
    if ([data1 isEqualToData:data2]) {
        return YES;
    }else{
        return NO;
    }
    
}

#pragma mark - Feedback button

- (IBAction)feedback_btn_click:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:kAppNameAlert message:@"All feedback is processed by Soft Intelligence, All information remains anonymous to the APM North East Branch, So Please be as honest as you can" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alert animated:YES completion:nil];
    });
}

#pragma mark ImagePicker Delegates

- (IBAction)takeAndAddPhoto:(id)sender
{
    
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:kAppNameAlert message:@"Please, Pick Photo from Camera or PhotoLibrary" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            
            [self performSelector:@selector(showcamera) withObject:nil afterDelay:0.0];
        }
        else
        {
            //[self showingAlert:@"Camera Not Found" :@"This Device has no Camera"];
            
            [self errorAlertWithTitle:@"Camera not found" message:@"This Device has no Camera" actionTitle:@"Dismiss"];
        }
        // Distructive button tapped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Photo Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // OK button tapped.
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        {
            
            UIImagePickerController *photoPicker = [[UIImagePickerController alloc] init];
            photoPicker.delegate = self;
            photoPicker.allowsEditing = NO;
            photoPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
          //  [self.navigationController.tabBarController presentViewController:photoPicker animated:YES completion:nil];
            [self presentViewController:photoPicker animated:YES completion:NULL];
        }
        
        //        [self dismissViewControllerAnimated:YES completion:^{
        //        }];
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
   
}


-(void)showcamera
{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusAuthorized)
    {
        [self popCamera];
    }
    else if(authStatus == AVAuthorizationStatusNotDetermined)
    {
        NSLog(@"%@", @"Camera access not determined. Ask for permission.");
        
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
         {
             if(granted)
             {
                 NSLog(@"Granted access to %@", AVMediaTypeVideo);
                 [self popCamera];
             }
             else
             {
                 NSLog(@"Not granted access to %@", AVMediaTypeVideo);
                 [self camDenied];
             }
         }];
    }
    else if (authStatus == AVAuthorizationStatusRestricted)
    {
        // My own Helper class is used here to pop a dialog in one simple line.
        //[Helper popAlertMessageWithTitle:@"Error" alertText:@"You've been restricted from using the camera on this device. Without camera access this feature won't work. Please contact the device owner so they can give you access."];
        
        
        [self errorAlertWithTitle:kAppNameAlert message:@"You've been restricted from using the camera on this device. Without camera access this feature won't work. Please contact the device owner so they can give you access." actionTitle:@"OK"];

    }
    else
    {
        [self camDenied];
    }
    
}

-(void)popCamera{
    UIImagePickerController * cameraPicker = [[UIImagePickerController alloc] init];
    
    cameraPicker.delegate = self;
    cameraPicker.allowsEditing = NO;
    cameraPicker.modalPresentationStyle = UIModalPresentationCurrentContext;
    cameraPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:cameraPicker animated:YES completion:nil];
    
}
- (void)camDenied
{
    NSLog(@"%@", @"Denied camera access");
    
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Error" message:@"It looks like your privacy settings are preventing us from accessing your camera to take Photos. You can fix this by doing the following:\n\n1. Touch the Settings button below to open the Settings of this app.\n\n2. Turn the Camera on.\n\n3. Open this app and try again." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * settingsAction=[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @try
        {
            NSLog(@"tapped Settings");
            BOOL canOpenSettings = (UIApplicationOpenSettingsURLString != NULL);
            if (canOpenSettings)
            {
                NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
               
                [[UIApplication sharedApplication]openURL:url options:@{} completionHandler:nil];
            }
        }
        @catch (NSException *exception)
        {
            
        }
        
    }];
    UIAlertAction * cancelAction=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:settingsAction];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
    
    //NSString *alertText;
    //NSString *alertButton;
    
//    BOOL canOpenSettings = (UIApplicationOpenSettingsURLString != NULL);
//    if (canOpenSettings)
//    {
//        alertText = @"It looks like your privacy settings are preventing us from accessing your camera to do barcode scanning. You can fix this by doing the following:\n\n1. Touch the Go button below to open the Settings of this app.\n\n2. Turn the Camera on.";
//        
//        alertButton = @"Go";
//    }
//    else
//    {
//        alertText = @"It looks like your privacy settings are preventing us from accessing your camera to do barcode scanning. You can fix this by doing the following:\n\n1. Close this app.\n\n2. Open the Settings app.\n\n3. Scroll to the bottom and select this app in the list.\n\n4. Touch Privacy.\n\n5. Turn the Camera on.\n\n6. Open this app and try again.";
//        
//        alertButton = @"OK";
//    }
//    
//    UIAlertView *alert = [[UIAlertView alloc]
//                          initWithTitle:@"Error"
//                          message:alertText
//                          delegate:self
//                          cancelButtonTitle:alertButton
//                          otherButtonTitles:nil];
//    alert.tag = 3491832;
//    [alert show];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    chosenImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
    self.imageVw.image = chosenImage;
    
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera)
    {
        UIImageWriteToSavedPhotosAlbum(self.imageVw.image, nil, nil, nil);
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissViewControllerAnimated:YES completion:nil];
    });
    self.deleteButton.hidden=NO;
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark Location Delegates

- (IBAction)sendLocation:(id)sender
{
   // [indicator startAnimating];
    hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    //hud.contentColor =[UIColor colorWithRed:255/255.0f green:193/255.0f blue:13/255.0f alpha:1];
    //hud.contentColor =khudColour;
    hud.backgroundView.style = MBProgressHUDBackgroundStyleSolidColor;
    //hud.backgroundView.color = [UIColor colorWithWhite:0.f alpha:0.1f];
    
    // Set the label text.
    hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
    
    if (self.rghtButton.tag == 1)
    {
        [self.rghtButton setBackgroundImage:[UIImage imageNamed:@"feedbackpin.png"] forState:UIControlStateNormal];
        self.rghtButton.tag = 0;
        [self.locationManager stopUpdatingLocation];
        self.locationText.text = @"";
        //[indicator stopAnimating];
        dispatch_async(dispatch_get_main_queue(), ^{
            [hud hideAnimated:YES];
        });
        latitude = @"";
        longitude = @"";
    }
    
    else
    {
        self.locationManager = [CLLocationManager new];
        
        self.locationManager.delegate = self;
        
        self.locationManager.distanceFilter = 80.0;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        
        if ([[[UIDevice currentDevice]systemVersion] intValue] >= 8)
        {
             if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]){
                [self.locationManager requestWhenInUseAuthorization];
            }
        }
        [self.locationManager startUpdatingLocation];
    }
    
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
    if (status == kCLAuthorizationStatusDenied)
    {
        //location denied, handle accordingly
        NSLog(@"Dont allow");
        //[indicator stopAnimating];
        dispatch_async(dispatch_get_main_queue(), ^{
            [hud hideAnimated:YES];
        });
        [self.rghtButton setBackgroundImage:[UIImage imageNamed:@"feedbackpin.png"] forState:UIControlStateNormal];
        self.rghtButton.tag = 0;
        [self.locationManager stopUpdatingLocation];
        self.locationText.text = @"";
        latitude = @"";
        longitude = @"";
        
        
    }
    else if (status == kCLAuthorizationStatusAuthorizedAlways || status == kCLAuthorizationStatusAuthorizedWhenInUse)
    {
        NSLog(@"Allow");
        
        //hooray! begin startTracking
    }
    
}


-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [geocoder cancelGeocode];

    [geocoder reverseGeocodeLocation:self.locationManager.location
                   completionHandler:^(NSArray *placemarks, NSError *error)
     {
         NSLog(@"reverseGeocodeLocation:completionHandler: Completion Handler called!");
         
         if (error)
         {
             NSLog(@"Geocode failed with error: %@", error);
             
             [self errorAlertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"Dismiss"];
             //[indicator stopAnimating];
             dispatch_async(dispatch_get_main_queue(), ^{
                 [hud hideAnimated:YES];
             });
             [self.locationManager stopUpdatingLocation];
             return;
             
         }
         
         CLPlacemark *placemark = [placemarks objectAtIndex:0];
         
         CLLocation *location = placemark.location;
         
         longitude = [[NSNumber numberWithDouble: location.coordinate.longitude] stringValue];
         latitude = [[NSNumber numberWithDouble: location.coordinate.latitude] stringValue];
         
         self.locationText.text = [NSString stringWithFormat:@"%@, %@, %@",placemark.locality,placemark.administrativeArea,placemark.country];
         [self.rghtButton setBackgroundImage:[UIImage imageNamed:@"feedbackpin.png"] forState:UIControlStateNormal];
         self.rghtButton.tag = 1;
         
         //[indicator stopAnimating];
         dispatch_async(dispatch_get_main_queue(), ^{
             [hud hideAnimated:YES];
         });
     }];
    
    [self.locationManager stopUpdatingLocation];
}


-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"%@",error.localizedDescription);
    //[indicator stopAnimating];
    dispatch_async(dispatch_get_main_queue(), ^{
        [hud hideAnimated:YES];
    });
    [self.locationManager stopUpdatingLocation];
    
    switch([error code])
    {
            
            
        case kCLErrorNetwork: // general, network-related error
        {
            
            
          [self errorAlertWithTitle:kAppNameAlert message:@"Please check your network connection or that you are not in airplane mode." actionTitle:@"OK"];
        }
            break;
        case kCLErrorDenied:{
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Background Location Access Disabled" message:@"In order to be notified, please open this app's settings and set location access to 'Always'." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction * settingsAction=[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                @try
                {
                    NSLog(@"tapped Settings");
                    BOOL canOpenSettings = (UIApplicationOpenSettingsURLString != NULL);
                    if (canOpenSettings)
                    {
                        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                         [[UIApplication sharedApplication]openURL:url options:@{} completionHandler:nil];
                    }
                }
                @catch (NSException *exception)
                {
                    
                }
                
            }];
            UIAlertAction * cancelAction=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
            
            [alert addAction:settingsAction];
            [alert addAction:cancelAction];
            [self presentViewController:alert animated:YES completion:nil];
            
            
        }
            break;
            
    }
}

#pragma mark DeleteButton Click

- (IBAction)deleteButtonClick:(id)sender {
    self.imageVw.image = [UIImage imageNamed:@"CameraNotAllowed.png"];
    self.deleteButton.hidden=YES;

}

-(void)errorAlertWithTitle:(NSString *)titleName message:(NSString *)message actionTitle:(NSString *)actionName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:message preferredStyle:UIAlertControllerStyleAlert];
    
    
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:UIAlertActionStyleCancel handler:nil];
    [alertCont addAction:okAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
    
}


//Use this for Post Request in AFNetworking 3.0
/*
 -(void)postRequest:(NSString *)urlStr parameters:(NSDictionary *)parametersDictionary completionHandler:(void (^)(NSString*, NSDictionary*))completionBlock{
    NSURL *URL = [NSURL URLWithString:urlStr];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    [manager POST:URL.absoluteString parameters:parametersDictionary progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseObject
                                                             options:kNilOptions
                                                               error:&error];
         NSLog(@"Response : %@",json);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}
*/

//NSURL *emailPostURL = [NSURL URLWithString:self.emailURL];
//
//AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
//[manager POST:emailPostURL.absoluteString parameters:parametersDictionary progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
//    NSError* error;
//    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
//    NSLog(@"Response : %@",json);
//    NSLog(@"value %@", [responseObject objectForKey:@"response"]);
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [standardUserDefaults setObject:[responseObject objectForKey:@"email"] forKey:@"emailId"];
//        [standardUserDefaults synchronize];
//        
//        hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//        hud.contentColor =khudColour;
//        // Set the custom view mode to show any view.
//        hud.mode = MBProgressHUDModeCustomView;
//        // Set an image view with a checkmark.
//        UIImage *image = [[UIImage imageNamed:@"CheckMark"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//        hud.customView = [[UIImageView alloc] initWithImage:image];
//        // Looks a bit nicer if we make it square.
//        hud.square = YES;
//        // Optional label text.
//        hud.label.text = NSLocalizedString(@"Done", @"HUD done title");
//        
//        [hud hideAnimated:YES afterDelay:1.5f];
//    });
//    
//} failure:^(NSURLSessionDataTask *task, NSError *error) {
//    NSLog(@"Error: %@", error);
//    dispatch_async(dispatch_get_main_queue(), ^{
//        /// NSUserDefaults * removeUD = [NSUserDefaults standardUserDefaults];
//        [standardUserDefaults removeObjectForKey:@"emailId"];
//        [[NSUserDefaults standardUserDefaults]synchronize ];
//        
//        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Something went wrong!" message:@"Please try again later" preferredStyle:UIAlertControllerStyleAlert];
//        UIAlertAction * okAction=[UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
//            [self dismissViewControllerAnimated:YES completion:nil];
//            
//            [self presentViewController:alertController animated:YES completion:nil];
//            
//        }];
//        [alert addAction:okAction];
//        [self presentViewController:alert animated:YES completion:nil];
//        
//        NSLog(@"response is null");
//    });
//    
//}];

- (BOOL)hasTopNotch {
    if (@available(iOS 11.0, *)) {
        return [[[UIApplication sharedApplication] delegate] window].safeAreaInsets.top > 20.0;
    }
    
    return  NO;
}

@end
