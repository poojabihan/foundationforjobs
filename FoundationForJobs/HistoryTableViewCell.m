//
//  HistoryTableViewCell.m
//  TellTheStationMaster
//
//  Created by Tarun Sharma on 09/03/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#import "HistoryTableViewCell.h"
@import QuartzCore;
@implementation HistoryTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    self.textOfLocation.adjustsFontSizeToFitWidth=YES;
    self.dateAndTime.adjustsFontSizeToFitWidth=YES;
    self.sendQueryButton.layer.cornerRadius = 8; // this value vary as per your desire
    self.sendQueryButton.clipsToBounds = YES;
    self.imagevw.layer.cornerRadius = 10.0f;
    self.imagevw.clipsToBounds = YES;
    
    [self shadowToView:_view];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) shadowToView:(UIView *)currentView
{
    currentView.layer.masksToBounds = NO;
    currentView.layer.cornerRadius = 10.0;
    
    currentView.layer.shadowRadius  = 10.0f;
    currentView.layer.shadowColor   = [UIColor grayColor].CGColor;
    currentView.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    currentView.layer.shadowOpacity = 0.3f;
    currentView.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath1      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(currentView.bounds, shadowInsets)];
    
    currentView.layer.shadowPath    = shadowPath1.CGPath;
    
}


@end
