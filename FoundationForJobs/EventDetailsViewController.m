//
//  EventDetailsViewController.m
//  APMNorthEast
//
//  Created by Tarun Sharma on 17/08/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#import "EventDetailsViewController.h"
#import "UIImageView+WebCache.h"
#import "UIColor+Hexadecimal.h"

@interface EventDetailsViewController ()
{ CGRect newFrame;
    
}
@end

@implementation EventDetailsViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    leftButton.frame = CGRectMake(0, 0, 30, 20);
    [leftButton addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];

    // Do any additional setup after loading the view.
 //   [self setTwoLineTitle:@"Event Details" color:[UIColor colorWithHex:@"#f4a300"] font:[UIFont boldSystemFontOfSize: 17.0f]];
    [self.eventTitleLabel setAdjustsFontSizeToFitWidth:YES];
    [self.subTitleLabel setAdjustsFontSizeToFitWidth:YES];
    [self.dateLabel setAdjustsFontSizeToFitWidth:YES];
    [self.timeLabel setAdjustsFontSizeToFitWidth:YES];
    [self.costLabel setNumberOfLines:0];
    [self.costLabel sizeToFit];
    [self.costLabel setNeedsDisplay];
    [self.cpdLabel setAdjustsFontSizeToFitWidth:YES];
    [self.venueLabel setAdjustsFontSizeToFitWidth:YES];
    [self.organizerNameLabel setAdjustsFontSizeToFitWidth:YES];
    
    NSLog(@"Btn Url - %@",_BookUrl);
    _Url_lbl_text.text=_BookUrl;
    if ([self.imagesArray count]!=0) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            // No explicit autorelease pool needed here.
            // The code runs in background, not strangling
            // the main run loop.
            [self loadingImages];
            //        dispatch_sync(dispatch_get_main_queue(), ^{
            //            // This will be called on the main thread, so that
            //            // you can update the UI, for example.
            //            [self longOperationDone];
            //        });
        });
    }
    else{
        self.upperViewHeightConstraint.constant = 0;
    }
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // No explicit autorelease pool needed here.
        // The code runs in background, not strangling
        // the main run loop.
        [self loadingElementsInEventDetails];
    });
    //    if ([str length]==0) {
    //
    //    }
    //    else{
    //
    //    }
}

-(void) goBack:(UIButton *) sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)loadingElementsInEventDetails{
    [self.eventTitleLabel setText:self.eventTitleString];
    [self.subTitleLabel setText:self.subtitleString];
    [self.dateLabel setText:[self.dateString isEqualToString:@""] ? @"Details not available" : self.dateString];
    [self.timeLabel setText:[self.timeString  isEqualToString:@""] ? @"Details not available" : self.timeString];
    [self.venueLabel setText:[self.venueString isEqualToString:@""] ? @"Details not available" : self.venueString];
    [self.cpdLabel setText:[self.cpdString isEqualToString:@""] ? @"Details not available" : self.cpdString];
    [self.costLabel setText:[self.costString isEqualToString:@""] ? @"Details not available" : self.costString];
    [self.organizerNameLabel setText:[self.organizerString isEqualToString:@""] ? @"Details not available" : self.organizerString];
    [self.summaryTextLabel setText:self.summaryString];
    
    CGSize maximumLabelSize = CGSizeMake(self.summaryTextLabel.frame.size.width, FLT_MAX);
    
    CGSize expectedLabelSize = [self.summaryString sizeWithFont:self.summaryTextLabel.font constrainedToSize:maximumLabelSize lineBreakMode:self.summaryTextLabel.lineBreakMode];
    
    //adjust the label the the new height.
    newFrame = self.summaryTextLabel.frame;
    newFrame.size.height = expectedLabelSize.height;
    self.constraintSummaryLbl.constant = expectedLabelSize.height;
    self.constraintLoweViewHeight.constant = newFrame.size.height + 490;
    self.tblEvent.tableHeaderView.frame = CGRectMake(0, 0, self.tblEvent.frame.size.width, newFrame.size.height + 850);
    
    [self shadowToView:_upperView shadow:10.0f];
    [self shadowToView:_lowerView shadow:100.0f];
//    self.lowerView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
//    self.lowerView.layer.borderWidth = 1.0;
    
  /*  dispatch_sync(dispatch_get_main_queue(), ^{
        self.lowerViewHeightConstraint.constant=self.eventTitleLabel.frame.size.height+self.subTitleLabel.frame.size.height+self.dateInfoView.frame.size.height+self.summaryTextLabel.frame.size.height+_Url_lbl_text.frame.size.height+_UrlbtnClick.frame.size.height+40;
        //[self longOperationDone];
    });*/
}

- (void) shadowToView:(UIView *)currentView shadow:(CGFloat)shadow
{
    currentView.layer.masksToBounds = NO;
    currentView.layer.cornerRadius = 10.0;
    
    currentView.layer.shadowRadius  = shadow;
    currentView.layer.shadowColor   = [UIColor grayColor].CGColor;
    currentView.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    currentView.layer.shadowOpacity = 0.3f;
    currentView.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath1      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(currentView.bounds, shadowInsets)];
    
    currentView.layer.shadowPath    = shadowPath1.CGPath;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark  NavigationTitle

- (void)setTwoLineTitle:(NSString *)titleText color:(UIColor *)color font:(UIFont *)font {
    CGFloat titleLabelWidth = [UIScreen mainScreen].bounds.size.width/2;
    UIView *wrapperView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, titleLabelWidth, 44)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, titleLabelWidth, 44)];
    label.backgroundColor = [UIColor clearColor];
    label.numberOfLines = 2;
    label.font = font;
    label.adjustsFontSizeToFitWidth = YES;
    label.textAlignment = UIBaselineAdjustmentAlignCenters;
    label.textColor = color;
    label.text = titleText;
    [wrapperView addSubview:label];
    self.navigationItem.titleView = wrapperView;
}
-(void)loadingImages{
    if ([self.imagesArray count]<10&&[self.imagesArray count]>1) {
        pageControl.numberOfPages=[self.imagesArray count];
    }
    else if([self.imagesArray count]>10){
        pageControl.numberOfPages=10;
    }
    else{
        dispatch_sync(dispatch_get_main_queue(), ^{
            pageControl.numberOfPages=0;
        });
    }
   
    dispatch_sync(dispatch_get_main_queue(), ^{
        imagesScrollView.contentSize=CGSizeMake(imagesScrollView.frame.size.width*[self.imagesArray count], imagesScrollView.frame.size.height);
        imagesScrollView.delegate = self;
    });
    
    for (int i =0; i<[self.imagesArray count]; i++)
    {
        
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(imagesScrollView.frame.size.width*i, 0, imagesScrollView.frame.size.width, imagesScrollView.frame.size.height)];
        [imageView setContentMode:UIViewContentModeScaleToFill];
        self.imageURL= [[NSString stringWithFormat:@"%@%@",kEventImageURL, [self.imagesArray objectAtIndex:i]]stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        CAShapeLayer * maskLayer = [CAShapeLayer layer];
        maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: imageView
                          .bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){8.0, 8.}].CGPath;
        
        imageView.layer.mask = maskLayer;

        UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:[NSURL URLWithString:self.imageURL].absoluteString];
        if(image == nil)
        {
            
            [imageView sd_setImageWithURL:[NSURL URLWithString:self.imageURL] placeholderImage:[UIImage imageNamed:@"ImgNotFound.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (error == nil) {
                    [imageView setImage:image];
                    
                } else {
                    NSLog(@"Image downloading error: %@", [error localizedDescription]);
                    [imageView setImage:[UIImage imageNamed:@"ImgNotFound.png"]];
                    
                }
            }];
            //            [imageView sd_setImageWithURL:[NSURL URLWithString:self.imageURL] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            //                if (error == nil) {
            //                    [imageView setImage:image];
            //
            //                } else {
            //                    NSLog(@"Image downloading error: %@", [error localizedDescription]);
            //                    [imageView setImage:[UIImage imageNamed:@"ImgNotFound.png"]];
            //
            //                }
            //            }];
        } else {
            [imageView setImage:image];
            
        }
        dispatch_sync(dispatch_get_main_queue(), ^{
            [imagesScrollView addSubview:imageView];
        });
    }
}
- (IBAction)changePage:(id)sender {
    [imagesScrollView scrollRectToVisible:CGRectMake(imagesScrollView.frame.size.width*pageControl.currentPage, imagesScrollView.frame.origin.y, imagesScrollView.frame.size.width, imagesScrollView.frame.size.height) animated:YES];
}
//-(void)viewDidLayoutSubviews{
//    NSLog(@"UIlabel Height %f",self.summaryTextLabel.frame.size.height);
//    
//    [self.parentScrollView setContentSize:CGSizeMake(self.parentScrollView.frame.size.width, self.lowerView.frame.origin.y + self.lowerView.frame.size.height + 10)];
//    
//    //        CGFloat axisY=0,height=0;
//    //        for (UIView *sub in self.self.bigScrollView.subviews)
//    //        {
//    //            height=sub.frame.size.height;
//    //            axisY=sub.frame.origin.y;
//    //        }self.bigScrollView.contentSize=CGSizeMake(self.bigScrollView.frame.size.width,height+axisY);
//    
//}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)sender {
    if (sender==imagesScrollView) {
        [self setIndiactorForCurrentPage];
    }
}
-(void)setIndiactorForCurrentPage
{
    uint page = imagesScrollView.contentOffset.x / imagesScrollView.frame.size.width;
    [pageControl setCurrentPage:page];
}

- (IBAction)ClickUrl:(id)sender{
    
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *URL = [NSURL URLWithString:_BookUrl];
    [application openURL:URL options:@{} completionHandler:^(BOOL success){
        if (success) {
            NSLog(@"Opened url");
        }
        else{
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:kAppNameAlert message:@"No Url" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:ok];
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self presentViewController:alert animated:YES completion:nil];
            });
        }
    }];
    
}
@end
