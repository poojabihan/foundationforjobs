//
//  NewsFeedListTableVC.h
//  APMNorthEast
//
//  Created by Tarun Sharma on 18/08/17.
//  Copyright © 2017 Chetaru Web LInk Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsFeedListTableVC : UIViewController
@property NSMutableArray * newsListArray;
@property NSString * getCurrentNewsURL,*newsMainImageURL;

@property (weak, nonatomic) IBOutlet UITableView *tblView;


@end
